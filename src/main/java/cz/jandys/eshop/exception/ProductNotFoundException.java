package cz.jandys.eshop.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class ProductNotFoundException extends ItaException {
    public ProductNotFoundException(String message) {
        super(message, HttpStatus.NOT_FOUND);
    }


}
