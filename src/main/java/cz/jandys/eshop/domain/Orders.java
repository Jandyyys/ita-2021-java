package cz.jandys.eshop.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import cz.jandys.eshop.enumerations.OrderStatus;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

/**
 * class Order
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */

@Entity
@Data
public class Orders extends AbstractEntity {
    private String name;
    private String address;
    @Enumerated(EnumType.STRING)
    private OrderStatus status = OrderStatus.NEW;


    @JsonManagedReference
    @ManyToMany
    @JoinTable(
            name = "product_orders",
            joinColumns = {@JoinColumn(name = "orders_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "product_id", referencedColumnName = "id")}
    )
    private List<Product> products;
}
