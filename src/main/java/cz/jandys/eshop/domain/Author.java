package cz.jandys.eshop.domain;

import lombok.Data;

import javax.persistence.Entity;

/**
 * class Author
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */
@Entity
@Data
public class Author extends AbstractEntity {
    private String name;
    private String bio;
    private Long birthDate;

//    @JsonManagedReference
//    @OneToMany
//    @JoinColumn(name = "author_id")
//    private List<Product> products;
}
