package cz.jandys.eshop.mapper;

import cz.jandys.eshop.domain.Orders;
import cz.jandys.eshop.model.OrdersDto;
import cz.jandys.eshop.model.OrdersRequestDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * interface OrderMapper
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */

@Mapper(uses = ProductMapper.class)
public interface OrdersMapper {

    Orders mapToEntity(OrdersRequestDto orderRequestDto);

    @Mapping(source = "status", target = "orderStatus")
    OrdersDto mapToResponse(Orders order);


}
