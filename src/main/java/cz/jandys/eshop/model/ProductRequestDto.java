package cz.jandys.eshop.model;


import cz.jandys.eshop.validation.ValidImageURL;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Data
public class ProductRequestDto {
    @NotNull
    @Size(max = 512)
    private String name;
    @NotNull
    @Size(max = 512)
    private String description;
    @NotNull
    @Positive
    private BigDecimal price;
    @PositiveOrZero
    private Long stock;
    @ValidImageURL
    private String image;
    @Positive
    private Long idAuthor;
    @Positive
    private Long idGenre;
}
