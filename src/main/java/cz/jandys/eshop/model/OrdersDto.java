package cz.jandys.eshop.model;

import cz.jandys.eshop.enumerations.OrderStatus;
import lombok.Data;

import java.util.List;

/**
 * class OrderDto
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */
@Data
public class OrdersDto {
    private Long id;
    private OrderStatus orderStatus;
    private List<ProductDto> products;
}
