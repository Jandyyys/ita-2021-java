package cz.jandys.eshop.mapper;

import cz.jandys.eshop.domain.Product;
import cz.jandys.eshop.model.ProductDto;
import cz.jandys.eshop.model.ProductRequestDto;
import org.mapstruct.Mapper;


@Mapper(uses = {AuthorMapper.class, GenreMapper.class})
public interface ProductMapper {

    Product mapToEntity(ProductRequestDto request);

    Product mapToEntity(ProductDto productDto);

    //    @Mappings({
//            @Mapping(target = "author", qualifiedByName = "NoAuthor"),
//            @Mapping(target = "genre", qualifiedByName = "NoGenre")
//    })
    ProductDto mapToResponse(Product product);

//    @Named("NoProducts")
//    @Mappings({
//            @Mapping(target = "author", expression = "java(null)"),
//            @Mapping(target = "genre", expression = "java(null)")
//    })
//    ProductDto mapToResponseNoProducts(Product product);

}
