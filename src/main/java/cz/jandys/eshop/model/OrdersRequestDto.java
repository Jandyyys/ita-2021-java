package cz.jandys.eshop.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

/**
 * class OrderRequestDto
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */

@Data
public class OrdersRequestDto {
    @Positive
    private Long idCart;
    @NotBlank
    private String name;
    @NotBlank
    private String address;
}
