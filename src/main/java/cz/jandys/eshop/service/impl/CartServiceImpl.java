package cz.jandys.eshop.service.impl;

import cz.jandys.eshop.domain.Cart;
import cz.jandys.eshop.domain.Product;
import cz.jandys.eshop.exception.ItaEntityNotFoundException;
import cz.jandys.eshop.factory.CartFactory;
import cz.jandys.eshop.mapper.CartMapper;
import cz.jandys.eshop.model.CartDto;
import cz.jandys.eshop.repository.CartRepository;
import cz.jandys.eshop.service.CartService;
import cz.jandys.eshop.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * class CartServiceImpl
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */


@Service
@RequiredArgsConstructor
public class CartServiceImpl implements CartService {

    private final static Logger log = LoggerFactory.getLogger(CartServiceImpl.class);

    private final ProductService productService;
    private final CartRepository cartRepository;
    private final CartMapper cartMapper;
    private final CartFactory cartFactory;

    @Transactional(readOnly = true)
    @Override
    public CartDto getCartByCartId(Long cartId) {

        log.info("Finding cart");

        final Cart cart = cartRepository.findById(cartId)
                .orElseThrow(
                        () -> new ItaEntityNotFoundException("Cart not found.")
                );
        return cartMapper.mapToResponse(cart);
    }

    @Transactional
    @Override
    public CartDto createNewCartWithProductId(Long productId) {
        final Product product = productService.getProductEntityById(productId);
        final Cart cart = new Cart();

        log.info("Creating new cart");

        List<Product> productList = List.of(product);
        cart.setProducts(productList);
        final Cart savedCart = cartRepository.save(cart);

        return cartMapper.mapToResponse(savedCart);
    }

    @Transactional
    @Override
    public CartDto updateExistingCartWithProductId(Long cartId, Long productId) {
        final Cart existingCart = cartRepository.getById(cartId);
        final Product product = productService.getProductEntityById(productId);
        existingCart.getProducts().add(product);

        log.info("Updating existing cart");

        final Cart savedCart = cartRepository.save(existingCart);

        return cartMapper.mapToResponse(savedCart);
    }
}
