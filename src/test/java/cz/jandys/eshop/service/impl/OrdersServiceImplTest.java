package cz.jandys.eshop.service.impl;

import cz.jandys.eshop.domain.Cart;
import cz.jandys.eshop.domain.Orders;
import cz.jandys.eshop.domain.Product;
import cz.jandys.eshop.exception.CartIsEmptyException;
import cz.jandys.eshop.exception.ItaException;
import cz.jandys.eshop.factory.OrdersFactory;
import cz.jandys.eshop.mapper.OrdersMapper;
import cz.jandys.eshop.model.OrdersDto;
import cz.jandys.eshop.model.OrdersRequestDto;
import cz.jandys.eshop.repository.CartRepository;
import cz.jandys.eshop.repository.OrdersRepository;
import org.assertj.core.api.WithAssertions;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OrdersServiceImplTest implements WithAssertions {

    @Mock
    private OrdersRepository ordersRepository;

    @Mock
    private CartRepository cartRepository;

    @Mock
    private OrdersMapper ordersMapper;

    @Mock
    private OrdersFactory ordersFactory;

    @InjectMocks
    private OrdersServiceImpl service;


    @Test
    void createNewOrder() {
        //arrange
        OrdersRequestDto request = new OrdersRequestDto()
                .setIdCart(1L);
        ArrayList<Product> products = Lists.newArrayList(new Product());
        Cart cart = new Cart().setProducts(products);
        Orders order = new Orders();
        OrdersDto expextedResult = new OrdersDto();

        when(cartRepository.findById(1L)).thenReturn(Optional.of(cart));
        when(ordersFactory.getNewOrder()).thenReturn(order);
        when(ordersRepository.save(order)).thenReturn(order);
        when(ordersMapper.mapToResponse(order)).thenReturn(expextedResult);

        //act
        final OrdersDto response = service.createNewOrder(request);

        //asserts
        assertThat(response).isNotNull();
        assertThat(response).isEqualTo(expextedResult);

        //verify
        verify(cartRepository).findById(1L);
        verify(ordersFactory).getNewOrder();
        verify(ordersRepository).save(order);
        verify(ordersMapper).mapToResponse(order);
    }

    @Test
    void createNewOrder_cartIsEmpty() {
        Cart cart = new Cart()
                .setProducts(new ArrayList<>());
        when(cartRepository.findById(1L)).thenReturn(Optional.of(cart));

        assertThrows(CartIsEmptyException.class, () -> service.createNewOrder(new OrdersRequestDto().setIdCart(1L)));
    }

    @Test
    void createNewOrder_cartNotFound() {
        when(cartRepository.findById(1L))
                .thenReturn(Optional.empty());

        assertThrows(ItaException.class, () -> service.createNewOrder(new OrdersRequestDto().setIdCart(1L)));

    }
}