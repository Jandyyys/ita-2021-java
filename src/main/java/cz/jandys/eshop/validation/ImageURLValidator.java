package cz.jandys.eshop.validation;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.ValidationException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * class ImageURLValidator
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */

public class ImageURLValidator implements ConstraintValidator<ValidImageURL, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) throws RuntimeException {
        if (value == null || value.isBlank()) {
            throw new ValidationException("Empty image URL");
        }
        Pattern pattern = Pattern.compile(".{1,}\\..{2,}");
        Matcher matcher = pattern.matcher(value);
        boolean b = matcher.matches();
        if (b) return b;
        throw new ValidationException("Syntax error wrong image URL");
    }
}
