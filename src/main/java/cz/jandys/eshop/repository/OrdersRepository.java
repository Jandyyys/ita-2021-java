package cz.jandys.eshop.repository;

import cz.jandys.eshop.domain.Orders;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * interface OrdersRepository
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */
public interface OrdersRepository extends JpaRepository<Orders, Long> {
}
