package cz.jandys.eshop.api;

import cz.jandys.eshop.model.AuthorDto;
import cz.jandys.eshop.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * class AuthorController
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */

@RequestMapping(value = "/api/v1/authors")
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class AuthorController {

    private final AuthorService authorService;

    @GetMapping
    public List<AuthorDto> getAllAuthors() {
        return authorService.getAllAuthors();
    }

    @GetMapping(value = "/{id}")
    public AuthorDto getAuthorbyId(@PathVariable Long id) {
        return authorService.getAuthorById(id);
    }

}
