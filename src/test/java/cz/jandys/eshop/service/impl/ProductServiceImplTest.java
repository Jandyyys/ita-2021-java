package cz.jandys.eshop.service.impl;

import cz.jandys.eshop.domain.Product;
import cz.jandys.eshop.exception.ProductNotFoundException;
import cz.jandys.eshop.mapper.ProductMapper;
import cz.jandys.eshop.model.ProductDto;
import cz.jandys.eshop.model.ProductRequestDto;
import cz.jandys.eshop.repository.ProductRepository;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class ProductServiceImplTest implements WithAssertions {

    @Mock
    private ProductRepository productRepository;

    @Mock
    private ProductMapper productMapper;

    @InjectMocks
    private ProductServiceImpl service;

    @Test
    void getAllProducts() {
        //arrange
        final Product expectedProduct1 = (Product) new Product()
                .setDescription("desc")
                .setImage("image")
                .setName("name")
                .setPrice(BigDecimal.valueOf(41))
                .setStock(2L)
                .setId(1L);

        final Product expectedProduct2 = (Product) new Product()
                .setDescription("desc")
                .setImage("image")
                .setName("name")
                .setPrice(BigDecimal.valueOf(45))
                .setStock(55L)
                .setId(2L);

        final ProductDto expectedResult = new ProductDto();
        when(productRepository.findAll()).thenReturn(List.of(expectedProduct1, expectedProduct2));
        when(productMapper.mapToResponse(expectedProduct1)).thenReturn(expectedResult);
        when(productMapper.mapToResponse(expectedProduct2)).thenReturn(expectedResult);


        //act
        final List<ProductDto> results = service.getAllProducts();


        //assert
        assertThat(results).isNotNull();
        assertThat(results).hasSize(2);
        assertThat(results).contains(expectedResult);


        //verify
        verify(productRepository).findAll();
        verify(productMapper).mapToResponse(expectedProduct1);
        verify(productMapper).mapToResponse(expectedProduct2);
    }

    @Test
    void getProductById() {
        //arrange
        final Product expectedProduct = (Product) new Product()
                .setDescription("desc")
                .setImage("image")
                .setName("name")
                .setPrice(BigDecimal.valueOf(41))
                .setStock(2L)
                .setId(1L);

        final ProductDto expectedResult = new ProductDto();
        when(productRepository.findById(1L)).thenReturn(Optional.of(expectedProduct));
        when(productMapper.mapToResponse(expectedProduct)).thenReturn(expectedResult);


        //act
        final ProductDto result = service.getProductById(1L);


        //assert
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(expectedResult);


        //verify
        verify(productRepository).findById(1L);
        verify(productMapper).mapToResponse(expectedProduct);
    }

    @Test
    void getProductById_notFound() {
        when(productRepository.findById(1L))
                .thenReturn(Optional.empty());

        assertThrows(ProductNotFoundException.class, () -> service.getProductById(1L));
    }


    @Test
    void getProductEntityById() {
        //arrange
        final Product expectedProduct = (Product) new Product()
                .setDescription("desc")
                .setImage("image")
                .setName("name")
                .setPrice(BigDecimal.valueOf(41))
                .setStock(2L)
                .setId(1L);

        when(productRepository.findById(1L)).thenReturn(Optional.of(expectedProduct));

        //act
        final Product result = service.getProductEntityById(1L);

        //assert
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(expectedProduct);

        //verify
        verify(productRepository).findById(1L);
    }

    @Test
    void getProductEntityById_notFound() {
        when(productRepository.findById(1L))
                .thenReturn(Optional.empty());

        assertThrows(ProductNotFoundException.class, () -> service.getProductEntityById(1L));
    }

    @Test
    void saveProduct() {
        //arrange
        ProductRequestDto expectedProduct = new ProductRequestDto()
                .setDescription("desc")
                .setImage("img")
                .setName("nam")
                .setPrice(BigDecimal.ONE)
                .setStock(1L);

        Product expectedResponse = (Product) new Product()
                .setDescription("desc")
                .setImage("image")
                .setName("name")
                .setPrice(BigDecimal.ONE)
                .setStock(10L)
                .setId(1L);

        ProductDto expectedReturn = new ProductDto()
                .setDescription("descOfReturn")
                .setId(1L)
                .setImage("img")
                .setName("nam")
                .setPrice(BigDecimal.TEN)
                .setStock(4L);

        when(productMapper.mapToEntity(expectedProduct)).thenReturn(expectedResponse);
        when(productMapper.mapToResponse(expectedResponse)).thenReturn(expectedReturn);
        when(productRepository.save(expectedResponse)).thenReturn(expectedResponse);


        //act
        final ProductDto response = service.saveProduct(expectedProduct);

        //assert
        assertThat(response).isNotNull();
        assertThat(response).isEqualTo(expectedReturn);


        verify(productRepository).save(expectedResponse);
        verify(productMapper).mapToEntity(expectedProduct);
        verify(productMapper).mapToResponse(expectedResponse);

    }

    @Test
    void updateProduct() {
        //arrange
        ProductRequestDto expectedProduct = new ProductRequestDto()
                .setDescription("desc")
                .setImage("img")
                .setName("nam")
                .setPrice(BigDecimal.ONE)
                .setStock(1L);

        Product expectedResponse = (Product) new Product()
                .setDescription("desc")
                .setImage("image")
                .setName("name")
                .setPrice(BigDecimal.ONE)
                .setStock(10L)
                .setId(1L);

        ProductDto expectedReturn = new ProductDto()
                .setDescription("descOfReturn")
                .setImage("img")
                .setName("nam")
                .setPrice(BigDecimal.TEN)
                .setStock(4L)
                .setId(1L);

        when(productRepository.existsById(1L)).thenReturn(true);
        when(productRepository.save(expectedResponse)).thenReturn(expectedResponse);
        when(productMapper.mapToEntity(expectedProduct)).thenReturn(expectedResponse);
        when(productMapper.mapToResponse(expectedResponse)).thenReturn(expectedReturn);

        //act
        final ProductDto response = service.updateProduct(expectedProduct, 1L);

        //asserts
        assertThat(response).isNotNull();
        assertThat(response).isEqualTo(expectedReturn);

        verify(productMapper).mapToResponse(expectedResponse);
        verify(productMapper).mapToEntity(expectedProduct);
        verify(productRepository).existsById(1L);
        verify(productRepository).save(expectedResponse);

    }

    @Test
    void updateProduct_notFound() {
        when(productRepository.existsById(1L))
                .thenReturn(false);

        assertThrows(ProductNotFoundException.class, () -> service.updateProduct(new ProductRequestDto(), 1L));
    }

    @Test
    void deleteProduct() {
        when(productRepository.existsById(1L))
                .thenReturn(false);

        assertThrows(EntityNotFoundException.class, () -> service.deleteProduct(1L));
    }

    @Test
    void deleteProduct_doesntExist() {
        when(productRepository.existsById(1L))
                .thenReturn(true);

        assertThrows(EntityNotFoundException.class, () -> service.deleteProduct(1L));

    }
}