package cz.jandys.eshop.service.impl;

import cz.jandys.eshop.domain.Cart;
import cz.jandys.eshop.domain.Orders;
import cz.jandys.eshop.domain.Product;
import cz.jandys.eshop.exception.CartIsEmptyException;
import cz.jandys.eshop.exception.ItaEntityNotFoundException;
import cz.jandys.eshop.factory.OrdersFactory;
import cz.jandys.eshop.mapper.OrdersMapper;
import cz.jandys.eshop.model.OrdersDto;
import cz.jandys.eshop.model.OrdersRequestDto;
import cz.jandys.eshop.repository.CartRepository;
import cz.jandys.eshop.repository.OrdersRepository;
import cz.jandys.eshop.service.OrdersService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * class OrderServiceImpl
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */

@Service
@RequiredArgsConstructor
public class OrdersServiceImpl implements OrdersService {

    private final static Logger log = LoggerFactory.getLogger(GenreServiceImpl.class);

    private final OrdersRepository ordersRepository;
    private final CartRepository cartRepository;
    private final OrdersMapper ordersMapper;
    private final OrdersFactory ordersFactory;

    @Transactional
    @Override
    public OrdersDto createNewOrder(OrdersRequestDto orderRequestDto) {
        Cart cart = cartRepository.findById(orderRequestDto.getIdCart())
                .orElseThrow(
                        () -> new ItaEntityNotFoundException("Cart not found")
                );

        log.info("Creating new order");

        if (cart.getProducts().isEmpty()) {
            throw new CartIsEmptyException("Cart is empty");
        }
        List<Product> productList = cart.getProducts();


        Orders order = ordersFactory.getNewOrder()
                .setProducts(productList)
                .setName(orderRequestDto.getName())
                .setAddress(orderRequestDto.getAddress());

        Orders savedOrder = ordersRepository.save(order);

        return ordersMapper.mapToResponse(savedOrder);

    }
}
