package cz.jandys.eshop.service;

import cz.jandys.eshop.domain.Genre;
import cz.jandys.eshop.model.GenreDto;

import java.util.List;

/**
 * interface GenreService
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */
public interface GenreService {

    List<GenreDto> getAllGenres();

    GenreDto getGenreById(Long id);

    Genre getGenreEntityById(Long id);

}
