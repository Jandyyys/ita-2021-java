package cz.jandys.eshop.repository;


import cz.jandys.eshop.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ProductRepository extends JpaRepository<Product, Long> {

}
