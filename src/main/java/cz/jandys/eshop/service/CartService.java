package cz.jandys.eshop.service;

import cz.jandys.eshop.model.CartDto;

/**
 * interface CartService
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */
public interface CartService {

    CartDto createNewCartWithProductId(Long productId);

    CartDto updateExistingCartWithProductId(Long cartId, Long productId);

    CartDto getCartByCartId(Long cartId);
}
