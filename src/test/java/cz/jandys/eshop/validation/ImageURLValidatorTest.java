package cz.jandys.eshop.validation;

import org.assertj.core.api.Assertions;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.validation.ValidationException;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertThrows;


@ExtendWith(MockitoExtension.class)
class ImageURLValidatorTest implements WithAssertions {

    private static final ImageURLValidator imageURLValidator = new ImageURLValidator();

    private static Stream<Arguments> testIsValid_validCases() {
        return Stream.of(
                Arguments.of("image.png"),
                Arguments.of("i.im"),
                Arguments.of("if.okf.fokjoj.fojkf"),
                Arguments.of("T.HisIsTo-tally_Legitimate/URL")
        );
    }

    private static Stream<Arguments> testIsValid_invalidCases() {
        return Stream.of(
                Arguments.of(" "),
                Arguments.of((Object) null),
                Arguments.of(".png"),
                Arguments.of("image"),
                Arguments.of("image."),
                Arguments.of("image.p"),
                Arguments.of("")
        );
    }


    @ParameterizedTest
    @MethodSource
    void testIsValid_invalidCases(String toBeTested) {
        assertThrows(ValidationException.class, () -> imageURLValidator.isValid(toBeTested, null));
    }


    @ParameterizedTest
    @MethodSource
    void testIsValid_validCases(String toBeTested) {
        final boolean result = imageURLValidator.isValid(toBeTested, null);

        Assertions.assertThat(result).isTrue();
    }
}