package cz.jandys.eshop.service.impl;

import cz.jandys.eshop.domain.Author;
import cz.jandys.eshop.exception.ItaEntityNotFoundException;
import cz.jandys.eshop.mapper.AuthorMapper;
import cz.jandys.eshop.model.AuthorDto;
import cz.jandys.eshop.repository.AuthorRepository;
import cz.jandys.eshop.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * class AuthorServiceImpl
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */

@Service
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService {

    private final static Logger log = LoggerFactory.getLogger(AuthorServiceImpl.class);

    private final AuthorRepository authorRepository;
    private final AuthorMapper authorMapper;


    @Transactional(readOnly = true)
    @Override
    public List<AuthorDto> getAllAuthors() {
        log.info("Finding All Authors");
        return authorRepository.findAll().stream()
                .map(authorMapper::mapToResponse)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public AuthorDto getAuthorById(Long id) {
        log.info("Finding Author");
        final Author author = authorRepository.findById(id)
                .orElseThrow(
                        () -> new ItaEntityNotFoundException("Author not found")
                );
        return authorMapper.mapToResponse(author);
    }

    @Transactional(readOnly = true)
    @Override
    public Author getAuthorEntityById(Long id) {
        log.info("Finding Author");
        return authorRepository.findById(id)
                .orElseThrow(
                        () -> new ItaEntityNotFoundException("Author not found")
                );
    }
}
