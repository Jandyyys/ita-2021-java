package cz.jandys.eshop.factory;

import cz.jandys.eshop.domain.Cart;
import org.springframework.stereotype.Component;

/**
 * class CartFactory
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */

@Component
public class CartFactory {
    public Cart getNewCart() {
        return new Cart();
    }
}
