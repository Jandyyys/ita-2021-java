insert into genre (id, name, description)
values (1, 'Novel',
        'Novels are books which have one long story written in them. They are works of prose fiction. They are longer than short stories and novellas');
insert into genre (id, name, description)
values (2, 'Myth',
        'A myth is a story which is not true. It means it is still subject to debate. Myths may be very old, or new (for example: urban myths).');
insert into genre (id, name, description)
values (3, 'Fairy tale',
        'A fairy tale is an English language expression for a kind of short story. It has the same meaning as the French expression conte de fée or Conte merveilleux');
insert into genre (id, name, description)
values (4, 'Epic poetry',
        'Epic poetry tells a dramatic story in a poem. There are characters in the story. It is usually long, and takes place in different settings. Epic poems started in prehistoric times as part of oral tradition. The Kyrgyz Epic of Manas is one of the longest written epics in history.');
insert into genre (id, name, description)
values (5, 'Science fiction',
        'Science fiction (often shortened to sci-fi or SF) is a kind of writing. Science fiction stories can be novels, movies, TV shows, video games, comic books and other literature.');
insert into genre (id, name, description)
values (6, 'Fantasy',
        'Fantasy is a genre of fiction (make believe) that shows some form of magic, or supernatural force. Often fantasy also means that the story happens in a fictional place, a world different from our own (e.g. Middle-earth or Narnia). ');
insert into genre (id, name, description)
values (7, 'Fable',
        'A fable is a type of story which shows something in life or has a meaning to a word. A fable teaches a lesson or suggests a moral from it. A fable starts in the middle of the story, that means, jumps into the main event without detailed introduction of characters.');

insert into author (id, name, birth_date, bio)
values (1, 'Antoine de Saint-Exupéry', -2208985139,
        'Antoine Marie Jean-Baptiste Roger, comte de Saint-Exupéry, simply known as de Saint-Exupéry, was a French writer, poet, aristocrat, journalist and pioneering aviator.');
insert into author (id, name, birth_date, bio)
values (2, 'Viktor Dyk', -2934745139,
        'Viktor Dyk was a nationalist Czech poet, prose writer, playwright, politician and political writer. He was sent to jail during the First World War for opposing the Austro-Hungarian empire.');
insert into author (id, name, birth_date, bio)
values (3, 'Oscar Wilde', -3660591539,
        'Oscar Fingal O''Flahertie Wills Wilde was an Irish poet and playwright. After writing in different forms throughout the 1880s, he became one of the most popular playwrights in London in the early 1890s.');
insert into author (id, name, birth_date, bio)
values (4, 'F. Scott Fitzgerald', -2335215539,
        'Francis Scott Key Fitzgerald was an American novelist, essayist, screenwriter, and short story writer. He was best known for his novels depicting the flamboyance and excess of the Jazz Age—a term which he popularized.');
insert into author (id, name, birth_date, bio)
values (5, 'Bohumil Hrabal', -1767221939,
        'Bohumil Hrabal was a Czech writer, often named among the best Czech writers of the 20th century.');
insert into author (id, name, birth_date, bio)
values (6, 'Vítězslav Nezval', -2208985139,
        'Vítězslav Nezval was one of the most prolific avant-garde Czech writers in the first half of the twentieth century and a co-founder of the Surrealist movement in Czechoslovakia.');
insert into author (id, name, birth_date, bio)
values (7, 'Jaroslav Seifert', -2177449139,
        'Jaroslav Seifert was a Nobel Prize–winning Czechoslovak writer, poet and journalist');
insert into author (id, name, birth_date, bio)
values (8, 'William Shakespeare', -12812165939,
        'William Shakespeare was an English playwright, poet, and actor, widely regarded as the greatest writer in the English language and the world''s greatest dramatist.');
insert into author (id, name, birth_date, bio)
values (9, 'George Orwell', -2114377139,
        'Eric Arthur Blair, known by his pen name George Orwell, was an English novelist, essayist, journalist and critic');
