package cz.jandys.eshop.repository;

import cz.jandys.eshop.domain.Genre;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * interface GenreRepository
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */
public interface GenreRepository extends JpaRepository<Genre, Long> {
}
