package cz.jandys.eshop.api;

import cz.jandys.eshop.model.ProductDto;
import cz.jandys.eshop.model.ProductRequestDto;
import cz.jandys.eshop.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RequestMapping(value = "/api/v1/product")
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @GetMapping
    public List<ProductDto> getAllProduts() {
        return productService.getAllProducts();
    }

    @GetMapping(value = "/{id}")
    public ProductDto getPruductById(@PathVariable("id") Long id) {
        return productService.getProductById(id);
    }

    @PostMapping
    public ProductDto saveProduct(@Valid @RequestBody ProductRequestDto request) {
        return productService.saveProduct(request);
    }

    @PutMapping(value = "/{id}")
    public ProductDto updateProduct(@PathVariable("id") Long id, @Valid @RequestBody ProductRequestDto request) {
        return productService.updateProduct(request, id);
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @DeleteMapping(value = "/{id}")
    public void deleteProduct(@PathVariable("id") Long id) {
        productService.deleteProduct(id);
    }

}
