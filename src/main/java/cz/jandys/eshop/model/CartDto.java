package cz.jandys.eshop.model;

import lombok.Data;

import java.util.List;

/**
 * class CartDto
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */

@Data
public class CartDto {
    private Long id;
    private List<ProductDto> products;
}
