package cz.jandys.eshop.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

/**
 * class Cart
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */

@Entity
@Data
public class Cart extends AbstractEntity {

    @JsonManagedReference
    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "product_carts",
            joinColumns = {@JoinColumn(name = "cart_id", referencedColumnName = "id", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "product_id", referencedColumnName = "id", nullable = false, updatable = false)}
    )
    private List<Product> products;
}
