package cz.jandys.eshop.api;

import cz.jandys.eshop.model.OrdersDto;
import cz.jandys.eshop.model.OrdersRequestDto;
import cz.jandys.eshop.service.OrdersService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * class OrderController
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */


@RequestMapping(value = "/api/v1/order")
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class OrdersController {

    private final OrdersService orderService;

    @PostMapping
    public OrdersDto createNewOrder(@Valid @RequestBody OrdersRequestDto orderRequestDto) {
        return orderService.createNewOrder(orderRequestDto);
    }


}
