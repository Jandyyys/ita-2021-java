package cz.jandys.eshop.service;

import cz.jandys.eshop.domain.Author;
import cz.jandys.eshop.model.AuthorDto;

import java.util.List;

/**
 * interface AuthorService
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */


public interface AuthorService {

    List<AuthorDto> getAllAuthors();

    AuthorDto getAuthorById(Long id);

    Author getAuthorEntityById(Long id);

}
