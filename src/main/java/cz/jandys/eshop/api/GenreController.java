package cz.jandys.eshop.api;

import cz.jandys.eshop.model.GenreDto;
import cz.jandys.eshop.service.GenreService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * class GenreController
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */

@RequestMapping(value = "/api/v1/genres")
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class GenreController {

    private final GenreService genreService;

    @GetMapping
    public List<GenreDto> getAllGenres() {
        return genreService.getAllGenres();
    }

    @GetMapping(value = "/{id}")
    public GenreDto getGenreById(@PathVariable Long id) {
        return genreService.getGenreById(id);
    }

}
