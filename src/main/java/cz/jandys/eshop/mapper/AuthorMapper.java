package cz.jandys.eshop.mapper;

import cz.jandys.eshop.domain.Author;
import cz.jandys.eshop.model.AuthorDto;
import org.mapstruct.Mapper;

/**
 * interface AuthorDto
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */
@Mapper(uses = {ProductMapper.class})
public interface AuthorMapper {

    Author mapToEntity(AuthorDto authorDto);


    //    @Mapping(target = "products", qualifiedByName = "NoProducts")
    AuthorDto mapToResponse(Author author);

//    @Named("NoAuthor")
//    @Mapping(target = "products", expression = "java(null)")
//    AuthorDto mapToResponseNoAuthor(Author author);


}
