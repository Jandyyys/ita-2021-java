package cz.jandys.eshop.model;

import lombok.Data;

/**
 * class AuthorDto
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */

@Data
public class AuthorDto {
    private Long id;
    private String name;
    private String bio;
    private Long birthDate;
//    private List<ProductDto> products;
}
