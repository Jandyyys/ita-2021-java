package cz.jandys.eshop.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@Slf4j
public class CommonExceptionHandeler extends ResponseEntityExceptionHandler {


    @ExceptionHandler(ItaException.class)
    public ResponseEntity<Object> handleItaException(ItaException e, ServletWebRequest webRequest) {
        log.error("An error occured during processing request + " + webRequest.getHttpMethod() + " at " + webRequest.getContextPath(), e);

        return handleExceptionInternal(e,
                new ExceptionResponseDto()
                        .setMessage(e.getMessage())
                        .setStatus(e.getStatus().value()),
                new HttpHeaders(),
                e.getStatus(),
                webRequest);
    }


    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleAnyException(Exception e, ServletWebRequest webRequest) {
        log.error("An error occured during processing request + " + webRequest.getHttpMethod() + " at " + webRequest.getContextPath(), e);

        return handleExceptionInternal(e, new ExceptionResponseDto()
                        .setMessage(e.getMessage())
                        .setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value()),
                new HttpHeaders(),
                HttpStatus.INTERNAL_SERVER_ERROR,
                webRequest);
    }
}
