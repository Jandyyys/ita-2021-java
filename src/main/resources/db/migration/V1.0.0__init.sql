create table author
(
    id          int8 not null,
    created_at  timestamp,
    modified_at timestamp,
    bio         varchar(255),
    birth_date  int8,
    name        varchar(255),
    primary key (id)
);
create table cart
(
    id          int8 not null,
    created_at  timestamp,
    modified_at timestamp,
    primary key (id)
);
create table genre
(
    id          int8 not null,
    created_at  timestamp,
    modified_at timestamp,
    description varchar(512),
    name        varchar(255),
    primary key (id)
);
create table orders
(
    id          int8 not null,
    created_at  timestamp,
    modified_at timestamp,
    address     varchar(255),
    name        varchar(255),
    status      varchar(255),
    primary key (id)
);
create table product
(
    id          int8 not null,
    created_at  timestamp,
    modified_at timestamp,
    description varchar(512),
    image       varchar(255),
    name        varchar(255),
    price       numeric(19, 2),
    stock       int8,
    author_id   int8,
    genre_id    int8,
    primary key (id)
);
create table product_carts
(
    cart_id    int8 not null,
    product_id int8 not null
);
create table product_orders
(
    orders_id  int8 not null,
    product_id int8 not null
);
create sequence hibernate_sequence start 1000 increment 1;
alter table if exists product
    add constraint FK_product_author foreign key (author_id) references author;
alter table if exists product
    add constraint FKl_product_genre foreign key (genre_id) references genre;
alter table if exists product_carts
    add constraint FK_product_carts_product foreign key (product_id) references product;
alter table if exists product_carts
    add constraint FK_product_carts_cart foreign key (cart_id) references cart;
alter table if exists product_orders
    add constraint FK_product_orders_product foreign key (product_id) references product;
alter table if exists product_orders
    add constraint FK_product_orders_orders foreign key (orders_id) references orders;