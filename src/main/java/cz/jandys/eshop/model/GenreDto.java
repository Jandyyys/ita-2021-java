package cz.jandys.eshop.model;

import lombok.Data;


/**
 * class Genre
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */

@Data
public class GenreDto {
    private Long id;
    private String name;
    private String description;
//    private List<ProductDto> products;
}
