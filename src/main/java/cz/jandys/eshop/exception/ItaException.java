package cz.jandys.eshop.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class ItaException extends RuntimeException {
    private HttpStatus status;

    public ItaException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }

    public ItaException(String message, HttpStatus status, Throwable cause) {
        super(message, cause);
        this.status = status;
    }
}
