package cz.jandys.eshop.exception;

import org.springframework.http.HttpStatus;

/**
 * class CartIsEmptyException
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */

public class CartIsEmptyException extends ItaException {
    public CartIsEmptyException(String message) {
        super(message, HttpStatus.NO_CONTENT);
    }
}
