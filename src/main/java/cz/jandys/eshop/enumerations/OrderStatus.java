package cz.jandys.eshop.enumerations;

/**
 * enum OrderStatus
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */
public enum OrderStatus {
    NEW, COMPLETED, CANCELLED
}
