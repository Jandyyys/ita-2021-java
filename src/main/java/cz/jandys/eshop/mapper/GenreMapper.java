package cz.jandys.eshop.mapper;

import cz.jandys.eshop.domain.Genre;
import cz.jandys.eshop.model.GenreDto;
import org.mapstruct.Mapper;

/**
 * interface GenreMapper
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */

@Mapper(uses = ProductMapper.class)
public interface GenreMapper {

    Genre mapToEntity(GenreDto genreDto);

    //    @Mapping(target = "products", qualifiedByName = "NoProducts")
    GenreDto mapToResponse(Genre genre);

//    @Named("NoGenre")
//    @Mapping(target = "products", expression = "java(null)")
//    GenreDto mapToResponseNoGenre(Genre genre);

}
