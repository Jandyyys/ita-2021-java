package cz.jandys.eshop.service;

import cz.jandys.eshop.domain.Product;
import cz.jandys.eshop.model.ProductDto;
import cz.jandys.eshop.model.ProductRequestDto;

import java.util.List;


public interface ProductService {

    List<ProductDto> getAllProducts();

    ProductDto getProductById(Long id);

    Product getProductEntityById(Long id);

    ProductDto saveProduct(ProductRequestDto request);

    ProductDto updateProduct(ProductRequestDto requestDto, Long id);

    void deleteProduct(Long id);
}
