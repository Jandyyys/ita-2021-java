package cz.jandys.eshop.factory;

import cz.jandys.eshop.domain.Orders;
import cz.jandys.eshop.enumerations.OrderStatus;
import org.springframework.stereotype.Component;

/**
 * class OrderFactory
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */

@Component
public class OrdersFactory {
    public Orders getNewOrder() {
        return new Orders().setStatus(OrderStatus.NEW);
    }
}
