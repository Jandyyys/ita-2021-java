package cz.jandys.eshop.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;

@Entity
@Data
@Accessors(chain = true)
public class Product extends AbstractEntity {
    private String name;
    @Column(length = 512)
    private String description;
    private BigDecimal price;
    private Long stock;
    private String image;

    @ManyToOne
    private Author author;

    @ManyToOne
    private Genre genre;

//    @ToString.Exclude
//    @JsonBackReference
//    @ManyToMany(mappedBy = "products")
//    private List<Cart> carts;
//
//    @ToString.Exclude
//    @JsonBackReference
//    @ManyToMany(mappedBy = "products")
//    private List<Orders> orders;

}
