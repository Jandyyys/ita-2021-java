package cz.jandys.eshop.service.impl;

import cz.jandys.eshop.domain.Product;
import cz.jandys.eshop.exception.ProductNotFoundException;
import cz.jandys.eshop.mapper.ProductMapper;
import cz.jandys.eshop.model.ProductDto;
import cz.jandys.eshop.model.ProductRequestDto;
import cz.jandys.eshop.repository.ProductRepository;
import cz.jandys.eshop.service.AuthorService;
import cz.jandys.eshop.service.GenreService;
import cz.jandys.eshop.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private static final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;
    private final AuthorService authorService;
    private final GenreService genreService;

    /**
     * Method getAllProducts()
     *
     * @return all of the products that are in "listOfProducts" class of List<ProductDto>
     */
    @Transactional(readOnly = true)
    @Override
    public List<ProductDto> getAllProducts() {

        log.info("Getting all products");
        return productRepository.findAll().stream()
                .map(productMapper::mapToResponse)
                .collect(Collectors.toList());
    }

    /**
     * Returns instance of ProductDto by ID that is in listOfProducts
     *
     * @param id chosen id of Product that should be returned
     * @return instance of ProductDto with representative ID
     */
    @Transactional(readOnly = true)
    @Override
    public ProductDto getProductById(Long id) {
        log.info("Searching for product");
        log.debug("Get product by id: {}", id);

        final Product product = productRepository.findById(id)
                .orElseThrow(
                        () -> new ProductNotFoundException("Product " + id + " was not found.")
                );

        return productMapper.mapToResponse(product);
    }

    @Transactional(readOnly = true)
    @Override
    public Product getProductEntityById(Long id) {
        log.info("Searching for product");
        log.debug("Get product by id: {}", id);

        return productRepository.findById(id)
                .orElseThrow(
                        () -> new ProductNotFoundException("Product " + id + " was not found.")
                );
    }

    @Transactional
    @Override
    public ProductDto saveProduct(ProductRequestDto request) {
        log.info("Saving product");
        log.debug("Saving product: {}", request);


        final Product product = productMapper.mapToEntity(request);
        if (request.getIdAuthor() != null)
            product.setAuthor(authorService.getAuthorEntityById(request.getIdAuthor()));
        if (request.getIdGenre() != null)
            product.setGenre(genreService.getGenreEntityById(request.getIdGenre()));

        final Product savedProduct = productRepository.save(product);


        return productMapper.mapToResponse(savedProduct);
    }


    @Transactional
    @Override
    public ProductDto updateProduct(ProductRequestDto requestDto, Long id) {
        log.info("Updating product");
        log.debug("Updating product by id: {}, product: {}", id, requestDto);

        if (productRepository.existsById(id)) {
            final Product requestProduct = (Product) productMapper.mapToEntity(requestDto).setId(id);
            if (requestDto.getIdAuthor() != null)
                requestProduct.setAuthor(authorService.getAuthorEntityById(requestDto.getIdAuthor()));
            if (requestDto.getIdGenre() != null)
                requestProduct.setGenre(genreService.getGenreEntityById(requestDto.getIdGenre()));
            final Product updatedProduct = productRepository.save(requestProduct);

            return productMapper.mapToResponse(updatedProduct);
        }
        throw new ProductNotFoundException("Product with id: " + id + " was not found.");
    }


    @Transactional
    @Override
    public void deleteProduct(Long id) {
        log.info("Deleting product");
        log.debug("Deleting product. id: {}", id);


        if (productRepository.existsById(id)) {
            productRepository.deleteById(id);
        }
        throw new EntityNotFoundException("Product with id: " + id + " was not found.");
    }


}
