package cz.jandys.eshop.mapper;

import cz.jandys.eshop.domain.Cart;
import cz.jandys.eshop.model.CartDto;
import org.mapstruct.Mapper;

/**
 * interface CartMapper
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */
@Mapper(uses = ProductMapper.class)
public interface CartMapper {

    Cart mapToEntity(CartDto cartDto);

    CartDto mapToResponse(Cart cart);

}
