package cz.jandys.eshop.api;

import cz.jandys.eshop.model.CartDto;
import cz.jandys.eshop.service.CartService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * class CartController
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */

@RequestMapping(value = "/api/v1/carts")
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class CartController {

    private final CartService cartService;

    @PostMapping(value = "/product/{productId}")
    public CartDto createNewCartWithProductId(@PathVariable Long productId) {
        return cartService.createNewCartWithProductId(productId);
    }

    @PutMapping(value = "/{cartId}/product/{productId}")
    public CartDto updateCartWithProductId(@PathVariable Long cartId, @PathVariable Long productId) {
        return cartService.updateExistingCartWithProductId(cartId, productId);
    }

    @GetMapping(value = "/{cartId}")
    public CartDto getCartById(@PathVariable Long cartId) {
        return cartService.getCartByCartId(cartId);
    }

}
