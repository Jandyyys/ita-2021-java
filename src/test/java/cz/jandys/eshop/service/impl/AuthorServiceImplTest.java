package cz.jandys.eshop.service.impl;

import cz.jandys.eshop.domain.Author;
import cz.jandys.eshop.exception.ItaException;
import cz.jandys.eshop.mapper.AuthorMapper;
import cz.jandys.eshop.model.AuthorDto;
import cz.jandys.eshop.repository.AuthorRepository;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AuthorServiceImplTest implements WithAssertions {

    @Mock
    private AuthorRepository authorRepository;

    @Mock
    private AuthorMapper authorMapper;

    @InjectMocks
    private AuthorServiceImpl service;

    @Test
    void getAllAuthors() {

        //arrange
        final Author testedAuthor = new Author()
                .setName("name")
                .setBio("Some bio")
                .setBirthDate(0L);

        final Author testedAuthor2 = new Author()
                .setName("otherAuthor")
                .setBio("Some anothr Bio")
                .setBirthDate(654654654L);

        final AuthorDto expectedResult = new AuthorDto();

        when(authorRepository.findAll()).thenReturn(List.of(testedAuthor, testedAuthor2));
        when(authorMapper.mapToResponse(testedAuthor)).thenReturn(expectedResult.setName("1"));
        when(authorMapper.mapToResponse(testedAuthor2)).thenReturn(expectedResult.setName("2"));

        //act
        final List<AuthorDto> authorDtos = service.getAllAuthors();


        //assert
        assertThat(authorDtos).hasSize(2);
        assertThat(authorDtos).contains(expectedResult.setName("1"));
        assertThat(authorDtos).contains(expectedResult.setName("2"));

        verify(authorRepository).findAll();
        verify(authorMapper).mapToResponse(testedAuthor);
        verify(authorMapper).mapToResponse(testedAuthor2);


    }

    @Test
    void getAuthorById() {
        //arrange
        final Author expectedAuthor = (Author) new Author()
                .setName("name")
                .setBio("bio")
                .setBirthDate(0L)
                .setId(1L);
//                .setProducts(null)

        final AuthorDto expectedResult = new AuthorDto();
        when(authorRepository.findById(1L)).thenReturn(Optional.of(expectedAuthor));
        when(authorMapper.mapToResponse(expectedAuthor)).thenReturn(expectedResult);


        //act
        final AuthorDto result = service.getAuthorById(1L);


        //assert
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(expectedResult);


        //verify
        verify(authorRepository).findById(1L);
        verify(authorMapper).mapToResponse(expectedAuthor);
    }

    @Test
    void getAuthorById_notFound() {
        when(authorRepository.findById(1L))
                .thenReturn(Optional.empty());

        assertThrows(ItaException.class, () -> service.getAuthorById(1L));
    }

    @Test
    void getAuthorEntityById() {
        //arrange
        final Author expectedAuthor = (Author) new Author()
                .setName("name")
                .setBio("bio")
                .setBirthDate(0L)
                .setId(1L);
//                .setProducts(null)

        when(authorRepository.findById(1L)).thenReturn(Optional.of(expectedAuthor));


        //act
        final Author result = service.getAuthorEntityById(1L);


        //assert
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(expectedAuthor);


        //verify
        verify(authorRepository).findById(1L);

    }

    @Test
    void getAuthorEntityById_notFound() {
        when(authorRepository.findById(1L))
                .thenReturn(Optional.empty());

        assertThrows(ItaException.class, () -> service.getAuthorEntityById(1L));
    }


}