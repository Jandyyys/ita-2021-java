package cz.jandys.eshop.service.impl;

import cz.jandys.eshop.domain.Genre;
import cz.jandys.eshop.exception.ItaException;
import cz.jandys.eshop.mapper.GenreMapper;
import cz.jandys.eshop.model.GenreDto;
import cz.jandys.eshop.repository.GenreRepository;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GenreServiceImplTest implements WithAssertions {

    @Mock
    private GenreRepository genreRepository;

    @Mock
    private GenreMapper genreMapper;

    @InjectMocks
    private GenreServiceImpl service;

    @Test
    void getAllGenres() {

        //arrange
        final Genre testedGenre = new Genre()
                .setName("name")
                .setDescription("desc");

        final Genre testedGenre2 = new Genre()
                .setName("otherAuthor")
                .setDescription("anotherDesc");

        final GenreDto expectedResult = new GenreDto();

        when(genreRepository.findAll()).thenReturn(List.of(testedGenre, testedGenre2));
        when(genreMapper.mapToResponse(testedGenre)).thenReturn(expectedResult.setName("1"));
        when(genreMapper.mapToResponse(testedGenre2)).thenReturn(expectedResult.setName("2"));

        //act
        final List<GenreDto> genreDtos = service.getAllGenres();


        //assert
        assertThat(genreDtos).hasSize(2);
        assertThat(genreDtos).contains(expectedResult.setName("1"));
        assertThat(genreDtos).contains(expectedResult.setName("2"));

        verify(genreRepository).findAll();
        verify(genreMapper).mapToResponse(testedGenre);
        verify(genreMapper).mapToResponse(testedGenre2);

    }

    @Test
    void getGenreById() {
        //arrange
        final Genre expectedGenre = (Genre) new Genre()
                .setName("name")
                .setDescription("desc")
                .setId(1L);

        final GenreDto expectedResult = new GenreDto();
        when(genreRepository.findById(1L)).thenReturn(Optional.of(expectedGenre));
        when(genreMapper.mapToResponse(expectedGenre)).thenReturn(expectedResult);


        //act
        final GenreDto result = service.getGenreById(1L);


        //assert
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(expectedResult);


        //verify
        verify(genreRepository).findById(1L);
        verify(genreMapper).mapToResponse(expectedGenre);
    }

    @Test
    void getGenreById_notFound() {
        when(genreRepository.findById(1L))
                .thenReturn(Optional.empty());

        assertThrows(ItaException.class, () -> service.getGenreById(1L));
    }

    @Test
    void getGenreEntityById() {
        //arrange
        final Genre expectedGenre = (Genre) new Genre()
                .setName("name")
                .setDescription("desc")
                .setId(1L);

        when(genreRepository.findById(1L)).thenReturn(Optional.of(expectedGenre));

        //act
        final Genre result = service.getGenreEntityById(1L);

        //assert
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(expectedGenre);

        //verify
        verify(genreRepository).findById(1L);
    }

    @Test
    void getGenreEntityById_notFound() {
        when(genreRepository.findById(1L))
                .thenReturn(Optional.empty());

        assertThrows(ItaException.class, () -> service.getGenreEntityById(1L));
    }
}