package cz.jandys.eshop.exception;

import org.springframework.http.HttpStatus;

/**
 * class ItaEntityNotFoundException
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */

public class ItaEntityNotFoundException extends ItaException {
    public ItaEntityNotFoundException(String message) {
        super(message, HttpStatus.NOT_FOUND);
    }
}
