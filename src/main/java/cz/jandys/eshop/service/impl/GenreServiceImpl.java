package cz.jandys.eshop.service.impl;

import cz.jandys.eshop.domain.Genre;
import cz.jandys.eshop.exception.ItaEntityNotFoundException;
import cz.jandys.eshop.mapper.GenreMapper;
import cz.jandys.eshop.model.GenreDto;
import cz.jandys.eshop.repository.GenreRepository;
import cz.jandys.eshop.service.GenreService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * class GenreServiceImpl
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */

@Service
@RequiredArgsConstructor
public class GenreServiceImpl implements GenreService {

    private final static Logger log = LoggerFactory.getLogger(GenreServiceImpl.class);


    private final GenreRepository genreRepository;
    private final GenreMapper genreMapper;

    @Transactional(readOnly = true)
    @Override
    public List<GenreDto> getAllGenres() {

        log.info("Finding All Genres");

        return genreRepository.findAll().stream()
                .map(genreMapper::mapToResponse)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public GenreDto getGenreById(Long id) {
        log.info("Finding Genre");

        Genre genre = genreRepository.findById(id)
                .orElseThrow(
                        () -> new ItaEntityNotFoundException("Genre not found")
                );
        return genreMapper.mapToResponse(genre);
    }

    @Transactional(readOnly = true)
    @Override
    public Genre getGenreEntityById(Long id) {
        log.info("Finding Genre");

        return genreRepository.findById(id)
                .orElseThrow(
                        () -> new ItaEntityNotFoundException("Genre not found")
                );
    }
}
