package cz.jandys.eshop.mapper;

import cz.jandys.eshop.domain.Product;
import cz.jandys.eshop.model.ProductDto;
import cz.jandys.eshop.model.ProductRequestDto;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class ProductMapperTest {

    @Mock
    private AuthorMapper authorMapper;

    @Mock
    private GenreMapper genreMapper;

    @InjectMocks
    private ProductMapperImpl productMapper;


    @Test
    void mapToEntity() {

        //arrange
        ProductRequestDto request = new ProductRequestDto()
                .setDescription("desc")
                .setName("name")
                .setImage("image")
                .setPrice(BigDecimal.valueOf(2))
                .setStock(1L);


        // act
        final Product product = productMapper.mapToEntity(request);


        // assert
        assertThat(product).isNotNull();
        SoftAssertions.assertSoftly(softAssertions -> {
            softAssertions.assertThat(product.getDescription()).isEqualTo(request.getDescription());
            softAssertions.assertThat(product.getImage()).isEqualTo(request.getImage());
            softAssertions.assertThat(product.getName()).isEqualTo(request.getName());
            softAssertions.assertThat(product.getPrice()).isEqualTo(request.getPrice());
            softAssertions.assertThat(product.getStock()).isEqualTo(request.getStock());
        });
    }

    @Test
    void mapToEntity_nullInput() {
        //act
        final Product product = productMapper.mapToEntity((ProductDto) null);

        //assert
        assertThat(product).isNull();
    }


    @Test
    void mapToResponse() {
        //arrange

        Product tryProduct = new Product()
                .setDescription("desc")
                .setName("name")
                .setImage("image")
                .setPrice(BigDecimal.valueOf(2))
                .setAuthor(null)
                .setGenre(null)
                .setStock((long) 1);

        //act
        final ProductDto productDto = productMapper.mapToResponse(tryProduct);

        assertThat(productDto).isNotNull();
        SoftAssertions.assertSoftly(softAssertions -> {
            softAssertions.assertThat(productDto.getDescription()).isEqualTo(tryProduct.getDescription());
            softAssertions.assertThat(productDto.getImage()).isEqualTo(tryProduct.getImage());
            softAssertions.assertThat(productDto.getName()).isEqualTo(tryProduct.getName());
            softAssertions.assertThat(productDto.getPrice()).isEqualTo(tryProduct.getPrice());
            softAssertions.assertThat(productDto.getStock()).isEqualTo(tryProduct.getStock());
            softAssertions.assertThat(productDto.getAuthor()).isEqualTo(tryProduct.getAuthor());
            softAssertions.assertThat(productDto.getGenre()).isEqualTo(tryProduct.getGenre());
        });
    }

    @Test
    void mapToResponse_nullInput() {
        //act
        final ProductDto productDto = productMapper.mapToResponse(null);

        //assert
        assertThat(productDto).isNull();
    }
}