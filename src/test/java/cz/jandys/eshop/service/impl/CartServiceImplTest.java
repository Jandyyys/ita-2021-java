package cz.jandys.eshop.service.impl;

import cz.jandys.eshop.domain.Cart;
import cz.jandys.eshop.domain.Product;
import cz.jandys.eshop.exception.ItaEntityNotFoundException;
import cz.jandys.eshop.factory.CartFactory;
import cz.jandys.eshop.mapper.CartMapper;
import cz.jandys.eshop.model.CartDto;
import cz.jandys.eshop.repository.CartRepository;
import cz.jandys.eshop.service.ProductService;
import org.assertj.core.api.WithAssertions;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CartServiceImplTest implements WithAssertions {

    @Mock
    private ProductService productService;

    @Mock
    private CartRepository cartRepository;

    @Mock
    private CartMapper cartMapper;

    @Mock
    private CartFactory cartFactory;

    @InjectMocks
    private CartServiceImpl service;


    @Test
    void getCartByCartId() {
        //arrange
        final Cart expectedCart = (Cart) new Cart()
                .setId(1L);

        final CartDto expectedResult = new CartDto();
        when(cartRepository.findById(1L)).thenReturn(Optional.of(expectedCart));
        when(cartMapper.mapToResponse(expectedCart)).thenReturn(expectedResult);

        //act
        final CartDto result = service.getCartByCartId(1L);

        //assert
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(expectedResult);

        //verify
        verify(cartRepository).findById(1L);
        verify(cartMapper).mapToResponse(expectedCart);
    }

    @Test
    void getCartByCartId_notFound() {
        when(cartRepository.findById(1L))
                .thenReturn(Optional.empty());

        assertThrows(ItaEntityNotFoundException.class, () -> service.getCartByCartId(1L));

    }


    @Test
    void createNewCartWithProductId() {

        //arrange
        final Product product = new Product();
        final Cart cart = new Cart();
        final Cart filledCart = new Cart()
                .setProducts(List.of(product));

        final CartDto expectedResult = new CartDto();

        final ArgumentCaptor<Cart> cartArgumentCaptor = ArgumentCaptor.forClass(Cart.class);

        when(productService.getProductEntityById(1L)).thenReturn(product);
        //when(cartFactory.getNewCart()).thenReturn(cart);
        when(cartRepository.save(cartArgumentCaptor.capture())).thenReturn(filledCart);
        when(cartMapper.mapToResponse(filledCart)).thenReturn(expectedResult);

        //act
        final CartDto result = service.createNewCartWithProductId(1L);

        //asserts
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(expectedResult);

        final Cart capturedCart = cartArgumentCaptor.getValue();
        assertThat(capturedCart.getProducts()).contains(product);

        //verify
        //verify(cartRepository).save(cart);
        verify(productService).getProductEntityById(1L);
        verify(cartMapper).mapToResponse(filledCart);

    }

    @Test
    void updateExistingCartWithProductId() {
        //arrange
        final Product product = new Product();
        final ArrayList<Product> products = Lists.newArrayList(product);
        final Cart cart = new Cart().setProducts(products);
        products.add(product);
        final Cart filledCart = cart.setProducts(products);
        final CartDto expectedResponse = new CartDto();

        when(cartRepository.getById(1L)).thenReturn(cart);
        when(productService.getProductEntityById(1L)).thenReturn(product);
        when(cartRepository.save(filledCart)).thenReturn(filledCart);
        when(cartMapper.mapToResponse(filledCart)).thenReturn((expectedResponse));

        //act
        final CartDto response = service.updateExistingCartWithProductId(1L, 1L);

        //asserts
        assertThat(response).isNotNull();
        assertThat(response).isEqualTo(expectedResponse);

        //verify
        verify(cartRepository).getById(1L);
        verify(cartRepository).save(filledCart);
        verify(cartMapper).mapToResponse(filledCart);
        verify(productService).getProductEntityById(1L);

    }
}
