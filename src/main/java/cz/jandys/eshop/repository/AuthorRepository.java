package cz.jandys.eshop.repository;

import cz.jandys.eshop.domain.Author;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * class AuthorRepository
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */

public interface AuthorRepository extends JpaRepository<Author, Long> {
}
