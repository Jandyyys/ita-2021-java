package cz.jandys.eshop.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * class Genre
 *
 * @Author Jakub Jandák, 2021
 */
@Data
@Entity
public class Genre extends AbstractEntity {
    private String name;
    @Column(length = 512)
    private String description;

//    @JsonManagedReference
//    @OneToMany
//    @JoinColumn(name = "genre_id")
//    private List<Product> products;
}
