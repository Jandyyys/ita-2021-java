package cz.jandys.eshop.service;

import cz.jandys.eshop.model.OrdersDto;
import cz.jandys.eshop.model.OrdersRequestDto;
import org.mapstruct.Mapping;

/**
 * interface OrderService
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */
public interface OrdersService {


    OrdersDto createNewOrder(OrdersRequestDto orderRequestDto);

}
