package cz.jandys.eshop.validation;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {ImageURLValidator.class})
public @interface ValidImageURL {

    String message() default "Image url syntax must be correct";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};


}
