package cz.jandys.eshop.repository;

import cz.jandys.eshop.domain.Cart;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * interface CartRepository
 *
 * @Date 08 2021
 * @Author Jakub Jandák
 */
public interface CartRepository extends JpaRepository<Cart, Long> {
}
